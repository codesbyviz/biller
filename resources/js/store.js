import Vue from 'vue';
import Vuex from 'vuex';
import Axios from 'axios';
Vue.use(Vuex);

const store = new Vuex.Store({
    state:{
        token:localStorage.getItem('accesstoken')||false,
        isLoading:false,
        isReady:false,
        isLoggedIn:false,
    },
    mutations:{
    },
    actions:{
        getProducts(context){
                Axios.defaults.headers.common['Authorization'] = 'Bearer '+context.state.token;
                return new Promise((resolve,reject)=>{
                Axios.get('/api/products?pagination=all').then(response=>{
                    resolve(response);
                }).catch(error=>{
                    reject(error);
                })
            }) 
        },
        Orders(context){
                Axios.defaults.headers.common['Authorization'] = 'Bearer '+context.state.token;
                return new Promise((resolve,reject)=>{
                Axios.get('/api/orders').then(response=>{
                    resolve(response);
                }).catch(error=>{
                    reject(error);
                })
            }) 
        },
        CreateProduct(context,data){
                Axios.defaults.headers.common['Authorization'] = 'Bearer '+context.state.token;
                return new Promise((resolve,reject)=>{
                Axios.post('/api/products',data).then(response=>{
                    resolve(response);
                }).catch(error=>{
                    reject(error);
                })
            }) 
        },
        ProductUpdate(context,data){
                Axios.defaults.headers.common['Authorization'] = 'Bearer '+context.state.token;
                return new Promise((resolve,reject)=>{
                Axios.put('/api/products/'+data.id,data).then(response=>{
                    resolve(response);
                }).catch(error=>{
                    reject(error);
                })
            }) 
        },
        Product(context,data){
                Axios.defaults.headers.common['Authorization'] = 'Bearer '+context.state.token;
                return new Promise((resolve,reject)=>{
                Axios.get('/api/products/'+data.id).then(response=>{
                    resolve(response);
                }).catch(error=>{
                    reject(error);
                })
            }) 
        },
        Products(context){
                Axios.defaults.headers.common['Authorization'] = 'Bearer '+context.state.token;
                return new Promise((resolve,reject)=>{
                Axios.get('/api/products').then(response=>{
                    resolve(response);
                }).catch(error=>{
                    reject(error);
                })
            }) 
        },
        Login(context, data){
            return new Promise((resolve,reject)=>{
                Axios.post('/api/login',{
                    username: data.username,
                    password: data.password,
                }).then(response=>{
                    const token = response.data.access_token;
                    localStorage.setItem('accesstoken',token);
                    resolve(response);
                }).catch(error=>{
                    reject(error);
                })
            }) 
        }
    },
    getters:{

    },
    
});

export default store;