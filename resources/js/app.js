require('./bootstrap');
window.Vue = require('vue');
import routes from './router';
import store from './store';
import VueRouter from 'vue-router';
import NProgress from 'nprogress';
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css';

window.Noty = require('noty');
Noty.overrideDefaults({theme : 'sunset',timeout:2500});


Vue.use(VueMaterial)

Vue.component('main-item', require('./components/Main.vue').default);

Vue.use(VueRouter);
const router = new VueRouter({
    mode: 'history',
    routes:routes
});


router.beforeResolve((to, from, next) => {
    NProgress.start();
    if (to.matched.some(record => record.meta.Auth)) {
      if (!store.state.token) {
        next({
          path: '/login',
          query: { redirect: to.fullPath }
        })
      } else {
        next()
      }
    } else {
      next();
    }
  })

router.afterEach((to, from) => {
    NProgress.done();
})
const app = new Vue({
  el: '#app',
  store,
  router
});